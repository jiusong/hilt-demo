package com.example.android.hilt.data

/**
 * Common interface for Logger data sources.
 *
 * Created by Jiusong.Gao on 10/16/20.
 * Copyright © 2020 Ticketmaster. All rights reserved.
 */
interface LoggerDataSource {
    fun addLog(msg: String)
    fun getAllLogs(callback: (List<Log>) -> Unit)
    fun removeLogs()
}